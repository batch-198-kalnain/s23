// console.log("Connected");

let trainer = {
	name: "Ash Ketchum",
	age: "10",
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friend: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	},
	talk: function() {
		console.log("Pikachu! I choose you");
	}
};
console.log(trainer);
console.log(trainer.name);
console.log(trainer.pokemon);
trainer.talk();
 
function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = level*3;
	this.attack = level*1.5;
	this.tackle = function(pokemonName, pokemonAttack) {
		console.log(pokemonName + " tackled " + this.name);
		let currentHP = this.health - pokemonAttack;
		if(currentHP <= 0) {
			console.log(this.name + " hp is reduced to " + currentHP);
			console.log(this.faint());
		} else {
			console.log(this.name + "'s health is now reduced to " + currentHP);
		}
	},
	this.faint = function() {
		console.log(this.name + "has fainted!");
	}
}

let pokemon1 = new Pokemon("Pikachu",12);
console.log(pokemon1);

let pokemon2 = new Pokemon("Geodude",8);
console.log(pokemon2);

let pokemon3 = new Pokemon("Mewtwo",100);
console.log(pokemon3);

pokemon1.tackle(pokemon2.name, pokemon2.attack);
pokemon2.tackle(pokemon3.name, pokemon3.attack);